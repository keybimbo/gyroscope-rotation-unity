﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Camera_Play : MonoBehaviour {

    private WebCamTexture webcam;
    public RawImage img;
    public Camera camera;


    float width;
    float height;

    // Use this for initialization
    void Start () {
        open_camera();
    }

    public void open_camera()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
        }

        if (devices.Length > 0)
        {
            webcam = new WebCamTexture(devices[0].name);
            img.texture = webcam;
            webcam.Play();
            StartCoroutine(getSize(webcam));
        }
    }

    IEnumerator getSize(WebCamTexture txt) {

        while (true)
        {
            if (webcam.width > 100) {
                width  = webcam.width;
                height = webcam.height;
                fit_camera();
                break;
            }
            yield return new WaitForSeconds(0.2f);
        }

        yield return null;
    }

    void fit_camera() {
        float new_h = 2 * camera.orthographicSize;
        float new_w = height * camera.aspect;
        img.SetNativeSize();
        float native_h = img.rectTransform.sizeDelta.y;
        float native_w = img.rectTransform.sizeDelta.x;
        img.rectTransform.sizeDelta = new Vector2((new_h*native_w)/native_h, new_h);
    }
}
