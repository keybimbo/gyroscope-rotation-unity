﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Gyroscope_rotate : MonoBehaviour
{

    public Transform target;
    Vector3 rot;
    Vector3 offset;

    float android_min = 0.1f;
    float sens = 5;

    bool android;


    void Start()
    {
        Input.gyro.enabled = true;
        #if UNITY_STANDALONE_WIN || UNITY_EDITOR
                android=false;
        #elif UNITY_ANDROID
                        android = true;
        #endif
    }

    void Update()
    {
        if (!android) {
            rot = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
            target.transform.RotateAround(target.position, rot, 120 * Time.deltaTime);
        }
        else {
            if ((Mathf.Abs(Input.gyro.rotationRateUnbiased.x) > android_min) || (Mathf.Abs(Input.gyro.rotationRateUnbiased.y) > android_min) || (Mathf.Abs(Input.gyro.rotationRateUnbiased.z) > android_min))
            {
                rot = new Vector3(Input.gyro.rotationRateUnbiased.x, Input.gyro.rotationRateUnbiased.y, Input.gyro.rotationRateUnbiased.z);
            }
            else
            {
                rot = new Vector3(0, 0, 0);
            };
            GameObject.Find("Debug").GetComponent<Text>().text = get_accell().x + "|" + get_accell().y + "|" + offset.y + "|" + offset.x  + "|";
            target.transform.eulerAngles = new Vector3 (get_accell().x - offset.x, get_accell().y - offset.y);
        }
    }

    public void reset() {
        offset = get_accell();
    }

    Vector3 get_accell() {
        return new Vector3(round_to(Input.acceleration.y * 180, sens) , round_to(Input.acceleration.x * 180,sens));
    }

    int round_to (float value,float to)
    {
        return (int)(Mathf.Ceil(value / to) * to);
    }
}
